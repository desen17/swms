
exports.helloFromLambdaHandler = async (event) => {
    const { httpMethod, path } = event;
    if (httpMethod !== 'GET') {
        throw new Error(`getAllItems only accept GET method, you tried: ${httpMethod}`);
    }
    console.log('received:', JSON.stringify(event));

    const response = {
        statusCode: 200,
        body: 'hello from lambda',
    };

    console.log(`response from: ${path} statusCode: ${response.statusCode} body: ${response.body}`);
    return response;
};