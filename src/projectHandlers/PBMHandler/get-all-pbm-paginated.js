// Create clients and set shared const values outside of the handler

// Create a DocumentClient that represents the query to get all items
const dynamodb = require('aws-sdk/clients/dynamodb');

const docClient = new dynamodb.DocumentClient();

// Get the DynamoDB table name from environment variables
const tableName = process.env.PBM_TABLE;

exports.getAllPBMPaginatedHandler = async (event) => {
    const { httpMethod, path ,body } = event;
    var input = JSON.parse(body);

    if (httpMethod !== 'POST') {
        throw new Error(`getAllItems only accept POST method, you tried: ${httpMethod}`);
    }
    // All log statements are written to CloudWatch by default. For more information, see
    // https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-logging.html
    console.log('received:', JSON.stringify(event));

    // get all items from the table (only first 1MB data, you can use `LastEvaluatedKey` to get the rest of data)
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB/DocumentClient.html#scan-property
    // https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Scan.html
    const params = { TableName: tableName};
  
    if(!input.hasOwnProperty('LastEvaluatedKey'))
    {
        params.ExclusiveStartKey = input.LastEvaluatedKey;
    }
    if(!input.hasOwnProperty('Limit'))
    {
        params.Limit = input.Limit;
    }
    
    
    /*dynamodb.scan(params, function scanUntilDone(err, data) {
      if (err) {
        console.log(err, err.stack);
      } else {
        // do something with data
    
        if (data.LastEvaluatedKey) {
          params.ExclusiveStartKey = data.LastEvaluatedKey;
    
          dynamodb.scan(params, scanUntilDone);
        } else {
          console.log("all data"+JSON.stringify(data));
        }
      }
    });*/
    const response = {};
    /*
    await docClient.scan(params, function (err, data) {
      if (err) {
          console.log("users::fetchOneByKey::error - " + JSON.stringify(err, null, 2));
          response.body(data);
      }
      else {
          console.log("users::fetchOneByKey::success - " + JSON.stringify(data, null, 2));
          response.body(err);
      }
  });
*/
    
   await docClient.scan(params).promise().then(result => {
      response.statusCode = 200;
      response.body = result;
    }).catch(e => {
      console.log(e);
      response.statusCode = e.statusCode;
      response.body = JSON.stringify(e);});
     
  return response;
};
