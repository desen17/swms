// Create clients and set shared const values outside of the handler

// Create a DocumentClient that represents the query to get an item
const dynamodb = require('aws-sdk/clients/dynamodb');

const docClient = new dynamodb.DocumentClient();
const tableName = process.env.PBM_TABLE;

exports.getPBMBySerialIdHandler = async (event) => {
    const { httpMethod, path, body } = event;
    if (httpMethod !== 'POST') {
        throw new Error(`getMethod only accept POST method, you tried: ${httpMethod}`);
    }
    var input = JSON.parse(body);
    const params = {
        TableName: tableName,
         ExpressionAttributeValues: {
            ':serialId': input.PBM_PROD_SERIAL_NUM
          },
        ExpressionAttributeNames: {
            "#pbmSerialId": "PBM_PROD_SERIAL_NUM",
        },
        ProjectionExpression: "PBM_PROD_BARCODE_ID, PBM_PROD_SERIAL_NUM, PBM_PROD_MFD_DATE,PBM_PROD_RECVD_DATE,PBM_PROD_SHORT_DESC,PBM_PROD_MAKE",
        FilterExpression: "#pbmSerialId = :serialId"
    };
     const response = {};
     await docClient.scan(params).promise().then(result => {response.body = JSON.stringify(result); response.statusCode = 200 }).catch(e => {console.log(e);
        response.body = JSON.stringify(e);});
    console.log(`response from: ${path} statusCode: ${response.statusCode} body: ${response.body}`);
    return response;
};
