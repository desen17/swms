// Create clients and set shared const values outside of the handler

// Create a DocumentClient that represents the query to get an item
const dynamodb = require('aws-sdk/clients/dynamodb');

const docClient = new dynamodb.DocumentClient();
const tableName = process.env.PBM_TABLE;

exports.getPBMByParamsHandler = async (event) => {
    const { httpMethod, path, body } = event;
    if (httpMethod !== 'POST') {
        throw new Error(`getMethod only accept POST method, you tried: ${httpMethod}`);
    }
    var input = JSON.parse(body);
    const expressionAttributeValues = {};
    const expressionAttributeNames = {};
    var filterexpression="";
    var keyname;
    var keyname2;
    const params = {
        TableName: tableName
        };

    //console.log('received:', JSON.stringify(event));
    //var input = JSON.parse(body);

    for (var key of Object.keys(input))
    {   
        keyname=":"+key;
        keyname2="#"+key;
        expressionAttributeValues[keyname] =  input[key];
        expressionAttributeNames[keyname2] = key;
    }
    console.log(JSON.stringify(expressionAttributeValues));
    console.log(JSON.stringify(expressionAttributeNames));
    for (var k of Object.keys(expressionAttributeNames)) 
    {
        var index = Object.keys(expressionAttributeNames).indexOf(k);
        keyname3= ":"+k.substring(1,k.length);
        //console.log(keyname3);
        if(filterexpression!="")
        {
            for(var f of Object.keys(expressionAttributeValues))
            {
                if(f==keyname3)
        filterexpression= filterexpression + " and " + k + " = " +  f;
            }
        }
        else
        {
            for(var f of Object.keys(expressionAttributeValues))
            {
                if(f==keyname3)
        filterexpression = k + " = " +  f;
        }
    }
        
    }
    //console.log(filterexpression);
      params.ExpressionAttributeValues = (expressionAttributeValues);
      params.ExpressionAttributeNames =  (expressionAttributeNames);
      params.FilterExpression = filterexpression;

      console.log(params);
     const response = {};
     await docClient.scan(params).promise().then(result => {response.body = JSON.stringify(result); response.statusCode = 200 }).catch(e => {console.log(e);
        response.body = JSON.stringify(e);});
    console.log(`response from: ${path} statusCode: ${response.statusCode} body: ${response.body}`);
    return response;
};
