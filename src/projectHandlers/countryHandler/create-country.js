
const dynamodb = require('aws-sdk/clients/dynamodb');
const uuid = require('uuid');
const docClient = new dynamodb.DocumentClient();
const tableName = process.env.COUNTRY_MASTER_TABLE;

exports.createCountryHandler = async (event) => {
    const { body, httpMethod, path } = event;
    if (httpMethod !== 'POST') {
        throw new Error(`postMethod only accepts POST method, you tried: ${httpMethod} method.`);
    }
    console.log('received:', JSON.stringify(event));

    var input = JSON.parse(body);
    if(!input.hasOwnProperty('CM_ID'))
    {
    var id = uuid.v1();
    input.CM_ID = id;
    input.CM_CREATED_DATE = new Date().toString();
    }
    input.CM_UPDATED_DATE = new Date().toString();
    
    const params = {
        TableName: tableName,
        Item: input,
    };
    await docClient.put(params).promise();

    const response = {
        statusCode: 200,
        body,
    };

    console.log(`response from: ${path} statusCode: ${response.statusCode} body: ${response.body}`);
    return response;
};
