
const dynamodb = require('aws-sdk/clients/dynamodb');

const docClient = new dynamodb.DocumentClient();
const tableName = process.env.COUNTRY_MASTER_TABLE;

exports.deleteCountryByIdHandler = async (event) => {
    const { httpMethod, path, body } = event;
    if (httpMethod !== 'POST') {
        throw new Error(`getMethod only accept POST method, you tried: ${httpMethod}`);
    }
    console.log('received:', JSON.stringify(event));
    var input = JSON.parse(body);
     const params = {
        TableName: tableName,
        Key: input
    };
    const { Item } = await docClient.delete(params).promise();

    const response = {
        statusCode: 200,
        body: JSON.stringify(Item),
    };

    console.log(`response from: ${path} statusCode: ${response.statusCode} body: ${response.body}`);
    return response;
};
