// Create clients and set shared const values outside of the handler

// Create a DocumentClient that represents the query to get an item
const dynamodb = require('aws-sdk/clients/dynamodb');

const docClient = new dynamodb.DocumentClient();
const tableName = process.env.DOCKDOOR_MASTER_TABLE;

exports.getDDByLOCIdHandler = async (event) => {
    const { httpMethod, path, body } = event;
    if (httpMethod !== 'POST') {
        throw new Error(`getMethod only accept POST method, you tried: ${httpMethod}`);
    }
    console.log('received:', JSON.stringify(event));
    var input = JSON.parse(body);
    const params = {
        TableName: tableName,
        ExpressionAttributeValues: {
            ':locId': input.DM_LOC_ID,
            ':atIn':input.DM_AT_IN
          },
        ExpressionAttributeNames: {
            "#Loc_Id": "DM_LOC_ID",
            "#At_In":"DM_AT_IN"
        },
        ProjectionExpression: "DM_ID, DM_LOC_ID, DM_AT_IN",
        FilterExpression: "#Loc_Id = :locId and #At_In = :atIn"
    };

     const response = {};
     await docClient.scan(params).promise().then(result => {response.body = JSON.stringify(result); response.statusCode = 200 }).catch(e => {console.log(e);
        response.body = JSON.stringify(e);});
    console.log(`response from: ${path} statusCode: ${response.statusCode} body: ${response.body}`);
    return response;
};
