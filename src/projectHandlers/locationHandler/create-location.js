// Create clients and set shared const values outside of the handler

// Create a DocumentClient that represents the query to add an item
const dynamodb = require('aws-sdk/clients/dynamodb');
const uuid = require('uuid');
const docClient = new dynamodb.DocumentClient();

// Get the DynamoDB table name from environment variables
const tableName = process.env.LOCATION_MASTER_TABLE;

exports.createLocationHandler = async (event) => {
    const { body, httpMethod, path } = event;
    if (httpMethod !== 'POST') {
        throw new Error(`postMethod only accepts POST method, you tried: ${httpMethod} method.`);
    }
    console.log('received:', JSON.stringify(event));

    var input = JSON.parse(body);
    if(!input.hasOwnProperty('LOC_ID'))
    {
    var id = uuid.v1();
    input.LOC_ID = id;
    input.LOC_CREATED_DATE = new Date().toString();
    }
    input.LOC_UPDATED_DATE = new Date().toString();
    
    const params = {
        TableName: tableName,
        Item: input,
    };
    await docClient.put(params).promise();

    const response = {
        statusCode: 200,
        body,
    };

    console.log(`response from: ${path} statusCode: ${response.statusCode} body: ${response.body}`);
    return response;
};
