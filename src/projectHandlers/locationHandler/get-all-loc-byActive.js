// Create clients and set shared const values outside of the handler

// Create a DocumentClient that represents the query to get an item
const dynamodb = require('aws-sdk/clients/dynamodb');

const docClient = new dynamodb.DocumentClient();
const tableName = process.env.LOCATION_MASTER_TABLE;

exports.getLocByActiveHandler = async (event) => {
    const { httpMethod, path, body } = event;
    if (httpMethod !== 'POST') {
        throw new Error(`getMethod only accept POST method, you tried: ${httpMethod}`);
    }
    console.log('received:', JSON.stringify(event));
    const params = {};
    var input = JSON.parse(body);
    if(!input.hasOwnProperty("LOC_ACTIVE"))
    {
        console.log("loc active exists");
        params.TableName = tableName;
    }
    else

   {  
       console.log("loc active exists");
       params.TableName = tableName,
       params.ExpressionAttributeValues = {
            ':active': input.LOC_ACTIVE
          },
       params.ExpressionAttributeNames = {
            "#IsActive": "LOC_ACTIVE",
        },
       params.ProjectionExpression= "LOC_ID, LOC_NAME, LOC_LTM_ID,LOC_ACTIVE",
       params.FilterExpression= "#IsActive = :active"
    };

     const response = {};
     await docClient.scan(params).promise().then(result => {response.body = JSON.stringify(result); response.statusCode = 200 }).catch(e => {console.log(e);
        response.body = JSON.stringify(e);});

    console.log(`response from: ${path} statusCode: ${response.statusCode} body: ${response.body}`);
    return response;
};
