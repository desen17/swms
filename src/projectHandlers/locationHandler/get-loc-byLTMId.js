// Create clients and set shared const values outside of the handler

// Create a DocumentClient that represents the query to get an item
const dynamodb = require('aws-sdk/clients/dynamodb');

const docClient = new dynamodb.DocumentClient();
const tableName = process.env.LOCATION_MASTER_TABLE;

exports.getLocByLTMIdHandler = async (event) => {
    const { httpMethod, path, body } = event;
    if (httpMethod !== 'POST') {
        throw new Error(`getMethod only accept POST method, you tried: ${httpMethod}`);
    }
    console.log('received:', JSON.stringify(event));
    var input = JSON.parse(body);
    const params = {
        TableName: 'LOCATION_MASTER',
        ExpressionAttributeValues: {
            ':locTypeId': input.LOC_LTM_ID
          },
        ExpressionAttributeNames: {
            "#LTM_Id": "LOC_LTM_ID",
        },
        ProjectionExpression: "LOC_ID, LOC_NAME, LOC_LTM_ID,LOC_ACTIVE",
        FilterExpression: "#LTM_Id = :locTypeId"
    };
 /* await docClient.scan(params, (error, result) => {
        if (error) {
           response = {
                statusCode: 200,
                body: JSON.stringify(error, null, 2),
            };
            console.log(JSON.stringify(error, null, 2))
        }
        else{
           response = {
                statusCode: 200,
                body: JSON.stringify(result),
            };
        }
      });
      */
     const response = {};
     await docClient.scan(params).promise().then(result => {response.body = JSON.stringify(result); response.statusCode = 200 }).catch(e => {console.log(e);
        response.body = JSON.stringify(e);});
      
   /*const response = {
    statusCode: 200
};
    await docClient.scan(params, (error, result) => {
        if (error) {
          response.body=JSON.stringify(error);
          console.log(JSON.stringify(response));
        }
        else{
            console.log("ëntering await",JSON.stringify(result));
          response.body=JSON.stringify(result);
          console.log(JSON.stringify(response));
        }
      });
      */

    console.log(`response from: ${path} statusCode: ${response.statusCode} body: ${response.body}`);
    return response;
};
