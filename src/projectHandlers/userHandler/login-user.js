// Create clients and set shared const values outside of the handler

// Create a DocumentClient that represents the query to get an item
const dynamodb = require('aws-sdk/clients/dynamodb');

const docClient = new dynamodb.DocumentClient();
const tableName = process.env.USER_MASTER_TABLE;

exports.loginUserHandler = async (event) => {
    const { httpMethod, path, body } = event;
    if (httpMethod !== 'POST') {
        throw new Error(`getMethod only accept POST method, you tried: ${httpMethod}`);
    }
    console.log('received:', JSON.stringify(event));
    var input = JSON.parse(body);
     const params = {
        TableName: tableName,
        ExpressionAttributeValues: {
            ':uname': input.UM_USERNAME,
            ':pwd' :  input.UM_PASSWORD
          },
        ExpressionAttributeNames: {
            "#password": "UM_PASSWORD",
            "#uname":"UM_USERNAME"
        },
        //KeyConditionExpression: 'UM_USERNAME = :uname',
        ProjectionExpression: 'UM_FIRSTNAME, UM_LASTNAME, UM_USERNAME',
        FilterExpression: '#password = :pwd and #uname = :uname'
    };
    const response = {};
     await docClient.scan(params).promise().then(result => {response.body = JSON.stringify(result); response.statusCode = 200 }).catch(e => {console.log(e);
        response.body = JSON.stringify(e); response.statusCode = e.statusCode});

    console.log(`response from: ${path} statusCode: ${response.statusCode} body: ${response.body}`);
    return response;
};
